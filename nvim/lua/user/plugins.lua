local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	PACKER_BOOTSTRAP = fn.system({
		"git",
		"clone",
		"--depth",
		"1",
		"https://github.com/wbthomason/packer.nvim",
		install_path,
	})
	vim.cmd([[packadd packer.nvim]])
	print("Packer installed. Please close and reopen Neovim!")
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
	return
end

-- Have packer use a popup window
packer.init({
	display = {
		open_fn = function()
			return require("packer.util").float({ border = "rounded" })
		end,
	},
})

-- Install your plugins here
return packer.startup(function(use)
	-- Packer can handle itself
	use({ "wbthomason/packer.nvim" })

	use({ "nvim-lua/plenary.nvim" }) --used by many plugins

	-- Notes plugin
	use({ "vimwiki/vimwiki" })

	-- Colorscheme
	use({ "ellisonleao/gruvbox.nvim", requires = { "rktjmp/lush.nvim" } })

	-- File explorer
	use({ "kyazdani42/nvim-tree.lua" })
	use({ "kyazdani42/nvim-web-devicons" })

	-- Cool FZF
	use({ "nvim-telescope/telescope.nvim" })

	-- Better syntax higlights and more
	use({ "nvim-treesitter/nvim-treesitter" })
	use({ "p00f/nvim-ts-rainbow" })
	use({ "JoosepAlviste/nvim-ts-context-commentstring" })

	-- Autopairs
	use({ "windwp/nvim-autopairs" })

	-- Git related
	use({ "lewis6991/gitsigns.nvim" })

	-- Statusline
	use({ "nvim-lualine/lualine.nvim" })

	-- Bufferline
	use({ "akinsho/bufferline.nvim" })
  -- Better buffer closing
	use({ "moll/vim-bbye" })

	-- Comments plugin
	use({ "numToStr/Comment.nvim" })

	-- Latex
	use({ "xuhdev/vim-latex-live-preview" })

	-- Screenshots
	use({ "krivahtoo/silicon.nvim" })

	-- Terminal
	use({ "akinsho/toggleterm.nvim" })

	use({ "lukas-reineke/indent-blankline.nvim" })

	-- Completion
	use({ "hrsh7th/nvim-cmp" })
	use({ "hrsh7th/cmp-buffer" })
	use({ "hrsh7th/cmp-path" })
	use({ "hrsh7th/cmp-cmdline" })
	use({ "hrsh7th/cmp-nvim-lua" })
	use({ "hrsh7th/cmp-nvim-lsp" })
	--Snippets
	use({ "L3MON4D3/LuaSnip" })
	use({ "saadparwaiz1/cmp_luasnip" })
	use({ "rafamadriz/friendly-snippets" })
	-- LSP, DAP, etc.
	use({ "williamboman/mason.nvim" })
	use({ "williamboman/mason-lspconfig.nvim" })
	use({ "neovim/nvim-lspconfig" })
	use({ "mfussenegger/nvim-dap" })
	use({ "jay-babu/mason-null-ls.nvim" })
	use({ "jose-elias-alvarez/null-ls.nvim" })

	if PACKER_BOOTSTRAP then
		require("packer").sync()
	end
end)
