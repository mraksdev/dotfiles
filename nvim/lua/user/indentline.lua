local status_ok, indent = pcall(require, "indent_blankline")
if not status_ok then
	return
end

vim.opt.list = true
vim.cmd([[highlight IndentBlanklineIndent1 guifg=#504945 gui=nocombine]])

indent.setup({
	char_highlight_list = {
		"IndentBlanklineIndent1",
	},
})
