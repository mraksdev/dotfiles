local options = {
	termguicolors = true,
	syntax = "on",
	showmode = false,
	exrc = true,
	number = true,
	relativenumber = true,
	numberwidth = 4,
	hlsearch = false,
	errorbells = false,
	expandtab = true,
	tabstop = 4,
	softtabstop = 4,
	shiftwidth = 4,
	autoindent = true,
	hidden = true,
	wrap = false,
	linebreak = true,
	ignorecase = true,
	smartcase = true,
	incsearch = true,
	scrolloff = 8,
	sidescrolloff = 10,
	pumheight = 20,
	signcolumn = "yes",
	colorcolumn = "80",
	cursorline = true,
	mouse = "a",
	splitbelow = true,
	splitright = true,
	updatetime = 300,
	swapfile = false,
	backup = false,
	undofile = true,
	undodir = vim.fn.glob("$HOME/.config/nvim/undodir"),
}

for option, value in pairs(options) do
	vim.o[option] = value
end

-- Autocmd
-- Fix LaTeX files
vim.cmd([[autocmd BufRead,BufNewFile *.tex set filetype=tex]])
-- Fix VimWiki filetype recognition
vim.cmd([[autocmd BufRead,BufNewFile *.wiki set filetype=wiki]])
-- Remove colorcolumn from some types
vim.cmd([[autocmd FileType text,markdown,html,xhtml,javascript,wiki,tex setlocal cc=0]])
-- Tab size for some types
vim.cmd([[
  autocmd FileType xml,html,xhtml,css,scss,javascript,lua,yaml setlocal shiftwidth=2 tabstop=2
]])
-- Remove endline spaces on save
vim.cmd([[au BufWritePre * :%s/\s\+$//e]])
