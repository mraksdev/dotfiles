local lsp_status_ok, mason_lsp = pcall(require, "mason-lspconfig")
if not lsp_status_ok then
	return
end

mason_lsp.setup_handlers({
	function(server_name)
		require("lspconfig")[server_name].setup({
			on_attach = require("user.lsp.handlers").on_attach,
			capabiliries = require("user.lsp.handlers").capabilities,
		})
	end,
})

mason_lsp.setup({
	ensure_installed = {
    "pyright",
	},
	automatic_installation = false,
})
