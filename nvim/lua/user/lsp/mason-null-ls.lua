local null_status_ok, mason_null_ls = pcall(require, "mason-null-ls")
if not null_status_ok then
  return
end

mason_null_ls.setup({
  ensure_installed = {
    "prettier",
    "black",
    "autopep8",
    "stylua",
    "shfmt",
    "flake8",
    "shellcheck",
  },
  automatic_setup = true,
})

mason_null_ls.setup_handlers()
