local opts = { noremap = true, silent = true }
local term_opts = { silent = true }
local keymap = vim.api.nvim_set_keymap

-- Space as leader key
vim.g.mapleader = " "

-- Exit insert mode
keymap("i", "jk", "<esc>", opts)

-- Splits
keymap("n", "<leader>sh", ":split<cr>", opts)
keymap("n", "<leader>sv", ":vsplit<cr>", opts)

-- Split navigatioin
keymap("n", "<c-h>", "<c-w>h", term_opts)
keymap("n", "<c-j>", "<c-w>j", term_opts)
keymap("n", "<c-k>", "<c-w>k", term_opts)
keymap("n", "<c-l>", "<c-w>l", term_opts)

-- Split resize
keymap("n", "<c-a-h>", "<c-w>5<", opts)
keymap("n", "<c-a-l>", "<c-w>5>", opts)
keymap("n", "<c-a-k>", "<c-w>+", opts)
keymap("n", "<c-a-j>", "<c-w>-", opts)

-- Keep cursor centered on next etc
keymap("n", "n", "nzzzv", opts)
keymap("n", "N", "Nzzzv", opts)
keymap("n", "J", "mzJ`z", opts)

-- Undo breakpoints
keymap("i", ",", ",<c-g>u", opts)
keymap("i", ".", ".<c-g>u", opts)
keymap("i", "!", "!<c-g>u", opts)
keymap("i", "?", "?<c-g>u", opts)

-- File manipulations
keymap("n", "<leader>sf", ":w<cr>", opts)
keymap("n", "<leader>Q", ":q<cr>", opts)
keymap("n", "<leader>ss", ":luafile %<cr>", opts)

-- Indenting
keymap("v", "<a-l>", ">gv", opts)
keymap("v", "<a-h>", "<gv", opts)
keymap("n", "<a-l>", "<s-v>>gv", opts)
keymap("n", "<a-h>", "<s-v><gv", opts)

-- Moving lines
keymap("v", "<a-j>", ":m '>+1<cr>gv=gv", opts)
keymap("v", "<a-k>", ":m '<-2<cr>gv=gv", opts)
keymap("i", "<a-j>", "<esc>:m .+1<cr>==", opts)
keymap("i", "<a-k>", "<esc>:m .-2<cr>==", opts)
keymap("n", "<a-j>", ":m .+1<cr>==", opts)
keymap("n", "<a-k>", ":m .-2<cr>==", opts)

-- Buffers
keymap("n", "<s-l>", ":bnext<cr>", opts)
keymap("n", "<s-h>", ":bprevious<cr>", opts)
keymap("n", "<leader>q", ":Bdelete<cr>", opts)

-- Keep yankbuffer on paste
keymap("v", "p", '"_dP', opts)

-- Exec scripts in different languages
keymap("n", "<leader>rp", ":!python %<cr>", opts)

-- Explorrer
keymap("n", "<leader>t", ":NvimTreeToggle<cr>", opts)
keymap("n", "<leader>e", ":NvimTreeFocus<cr>", opts)

-- Telescope
keymap("n", "<leader>ff", ":Telescope find_files<cr>", opts)
keymap("n", "<leader>fg", ":Telescope live_grep<cr>", opts)
keymap("n", "<leader>fb", ":Telescope buffers<cr>", opts)
keymap("n", "<leader>fh", ":Telescope help_tags<cr>", opts)
keymap("n", "<leader>/", ":Telescope current_buffer_fuzzy_find theme=ivy<cr>", opts)

-- Autoformatin null-ls
keymap("n", "<c-f>", ":lua vim.lsp.buf.format({ timeout_ms = 3000 })<cr>", opts)

-- Silicon screenshots
keymap("n", "<leader>sc", ":Silicon /home/alex/Pictures/code_screenshot.png<cr>", opts)
keymap("v", "<leader>sc", ":'<,'>Silicon /home/alex/Pictures/code_screenshot.png<cr>", opts)
