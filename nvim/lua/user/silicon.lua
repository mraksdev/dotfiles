local status_ok, silicon = pcall(require, "silicon")
if not status_ok then
	return
end

silicon.setup({
	font = "Hack Nerd Font=16",
	theme = "gruvbox",
	tab_width = 2,
	pad_horiz = 20,
	pad_vert = 20,
	line_number = true,
	line_pad = 2,
	line_offset = 1,
	round_corner = true,
	window_controls = true,
	shadow = {
		blur_radius = 0.0,
		offset_x = 0,
		offset_y = 0,
		color = "#555",
	},
	background = "#eff",
})
