#!/bin/sh
dir=$(pwd)

echo "create xorg conf"
rm -rfv ~/.config/X11
ln -s "$dir"/X11 ~/.config/X11

echo "create zsh conf"
rm -rfv ~/.config/zsh /etc/zsh
sudo rm -rfv /etc/zsh/zshenv
ln -s "$dir"/zsh ~/.config/zsh
sudo ln -s "$dir"/zsh/zshenv /etc/zsh/zshenv

echo "create hardware conf"
sudo rm -rfv /etc/X11/xorg.conf.d
sudo ln -s "$dir"/xorg.conf.d /etc/X11/xorg.conf.d

echo "create alacritty config"
rm -rfv ~/.config/alacritty
ln -s "$dir"/alacritty ~/.config/alacritty

echo "create awesome config"
rm -rfv ~/.config/awesome
ln -s "$dir"/awesome ~/.config/awesome

echo "create lazigit config"
rm -rfv ~/.config/lazygit
ln -s "$dir"/lazygit ~/.config/lazygit

echo "create lazigit config"
rm -rfv ~/.config/lf
ln -s "$dir"/lf ~/.config/lf

echo "unlimited mobile hotspot"
sudo ln -s "$dir"/ttl/99_default_ttl.conf /etc/sysctl.d/99_default_ttl.conf
sudo sysctl --system

echo "nvim config"
rm -rfv ~/.config/nvim
ln -s "$dir"/nvim ~/.config/nvim
