alias v='nvim'

# Pacman
alias pman='sudo pacman'
alias update='pman -Syyu'
alias upgrade='yay -Syyu'
alias unused='yay -Qdt'

# Archives
alias untar='tar -zxvf'

# mpv
alias mpvhd='mpv --ytdl-format="bestvideo[ext=mp4][height<=?720]+bestaudio[ext=m4a]"'
alias mpvfhd='mpv --ytdl-format="bestvideo[ext=mp4][height<=?1080]+bestaudio[ext=m4a]"'
