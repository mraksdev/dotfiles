# ls aliases
alias ls='lsd --group-dirs first'
alias lsa='ls -a'
alias ll='ls -lh'
alias la='ll -a'

# mkdir aliases
alias mkdir='mkdir -p'

# cd aliases
alias ..='cd ..'

# shutdown
alias sdn='sudo shutdown -h now'
